<?php

$id=$_GET['id']; 
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Acualizacion</title>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>

	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	
</head>
<body>
	<?php 
			require_once('header.php');
		 ?>

<div class="container">
  <h2>Actualiza CLiente</h2>
  <form action="update.php" method="POST">
    <div class="form-group">
    <?php $sql = "SELECT * FROM cli_clientes WHERE cli_id=$id";
						  $result = $mysqli->query($sql);

						  while($row = $result->fetch_assoc()){ 

						  
					 ?>

      <label for="text">Nombre:</label>
      <input type="text" class="form-control" id="cli_nombre"  name="cli_nombre" autofocus value="<?php echo $row['cli_nombre']?>">
      <label for="text">Latitud:</label>
      <input type="text" class="form-control" id="cli_latitud"  name="cli_latitud" autofocus value="<?php echo $row['cli_latitud']?>">
       <label for="text">Longitud:</label>
      <input type="text" class="form-control" id="cli_longitud"  name="cli_longitud" autofocus value="<?php echo $row['cli_longitud']?>">
      <input type="hidden" name="cli_id" id="cli_id" value="<?php echo $row['cli_id']; ?>">
    </div>
                          <?php }?>
                          
    
                          <button type="submit" class="btn btn-outline-primary">Actualiza</button>
                           <button type="submit" class="btn btn-success" href="clientes.php">Cancelar</button>
  </form>
</body>

</html>