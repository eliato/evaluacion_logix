
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Evaluacion Logix</title>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>

	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	
</head>
<body>
	<?php require_once('header.php');?>	

<div class="container">
	<h2>Lista Clientes</h2>
	
		<div>
		</div>
		
			<div class="col-xs-4">
				
			  <button class="btn btn-outline-success"><a href="register.php" >Nuevo</a></button>
			</div>
			
		
	
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Id</th>
					<th>nombre</th>
					<th>Latitud</th>
					<th>Longitud</th>
					<th>FechaCreacion</th>
					<th>Accion</th>
					
					
				</tr>
				<tbody>
					<?php $sql = "SELECT * FROM cli_clientes WHERE cli_estado=1 ";
						  $result = $mysqli->query($sql);

						  while($row = $result->fetch_assoc()){ 

						  
					 ?>

					
					<tr>
						<td>  <?php echo $row['cli_id']; ?> </td>
						<td><?php echo $row['cli_nombre']; ?></td>
						<td><?php echo $row['cli_latitud']; ?></td>
						<td><?php echo $row['cli_longitud']; ?></td>
						<td><?php echo $row['cli_fecha_creacion']; ?></td>
						
						
						<td><a href="actualiza.php?id=<?php echo $row['cli_id']?>"><span class="badge badge-pill badge-primary">Editar</span></a>
						<a href="delete.php?id=<?php echo $row['cli_id']?>"><span class="badge badge-pill badge-danger">Eliminar</span></a> </td>
					</tr>
					<?php } ?>
				</tbody>

			</thead>
		</table>

	</div>	

</div>
</body>
</html>