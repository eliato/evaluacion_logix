﻿# Host: 127.0.0.1  (Version 5.7.29-0ubuntu0.18.04.1)
# Date: 2020-03-01 01:00:25
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "cli_clientes"
#

CREATE TABLE `cli_clientes` (
  `cli_id` int(11) NOT NULL AUTO_INCREMENT,
  `cli_nombre` varchar(75) DEFAULT NULL COMMENT 'nombre del cliente',
  `cli_latitud` varchar(12) DEFAULT NULL COMMENT 'latitud del cliente',
  `cli_longitud` varchar(12) DEFAULT NULL COMMENT 'longitud del cliente',
  `cli_coordenada` varchar(25) DEFAULT NULL COMMENT 'coordenada del cliente',
  `cli_fecha_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `cli_ultima_modificacion` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `cli_estado` tinyint(2) DEFAULT '1' COMMENT '1=activo, 2=inactivo',
  PRIMARY KEY (`cli_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='tabla de clientes';

#
# Data for table "cli_clientes"
#

INSERT INTO `cli_clientes` VALUES (1,'CLIENTE 1','13.705243','-89.24233','13.705243,-89.24231','2020-02-29 12:20:10','2020-02-29 16:55:25',1),(2,'CLIENTE 2','13.696674','-89.197927','13.696674,-89.197927','2020-02-29 12:27:08','2020-02-29 16:56:12',1),(3,'CLIENTE 3','14.692511','-87.86136','14.692511,-87.86136','2020-02-29 12:27:12',NULL,1),(4,'CLIENTE 4','12.022747','-86.252007','12.022747, -86.252007','2020-02-29 12:27:16','2020-02-29 16:56:14',1),(5,'CLIENTE 5','8.103289','-80.596013','8.103289,-80.596013','2020-02-29 12:27:29','2020-02-29 16:56:15',1),(6,'CLIENTE 6','13.7123533','-89.1560526','13.7123533,-89.1560526','2020-02-29 16:38:15','2020-02-29 16:56:15',1);

#
# Structure for table "usu_usuarios"
#

CREATE TABLE `usu_usuarios` (
  `usu_id` int(4) NOT NULL AUTO_INCREMENT,
  `usu_nombre` varchar(35) DEFAULT NULL COMMENT 'nombre del usuario',
  `usu_password` varchar(150) DEFAULT NULL COMMENT 'contraseña del usuario',
  `usu_stado` tinyint(1) DEFAULT '1' COMMENT '1=activo, 2=inactivo',
  `usu_ultimo_ingreso` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`usu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "usu_usuarios"
#

INSERT INTO `usu_usuarios` VALUES (1,'prueba','827ccb0eea8a706c4c34a16891f84e7b',1,NULL);
