
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Formulario de Conversion</title>
	<meta charset="utf-8">
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>

	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	
</head>
<body>
	<?php 
			require_once('header.php');
		 ?>

<div class="container">
  <h2>Nuevo Cliente</h2>
  <button type="submit" class="btn btn-success" ><a href="clientes.php" >Atras</a></button>
  <form action="save.php" method="POST">
    <div class="form-group">
      <label for="text">Nombre:</label>
      <input type="text" class="form-control" id="cuota" placeholder="Ingrese su Cuota" name="nombre" autofocus required>
    </div>
    
    <div class="form-group">
      <label for="text">latitud:</label>
       <input type="text" class="form-control" id="latitud" placeholder="Ingrese latitud" name="latitud" autofocus required>
    
    </div>
    <div class="form-group">
      <label for="text">Longitud:</label>
       <input type="text" class="form-control" id="longitud" placeholder="Ingrese longitud" name="longitud" autofocus required>
      
      <p id="demo"></p>
    </div>
    
    
    <button type="submit" class="btn btn-primary">Guardar</button>
    
  </form>

  <br>
</div>


</body>

</html>