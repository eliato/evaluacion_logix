<?php


$username="root";
$password="12345678";
$database="logix";

function parseToXML($htmlStr)
{
$xmlStr=str_replace('<','&lt;',$htmlStr);
$xmlStr=str_replace('>','&gt;',$xmlStr);
$xmlStr=str_replace('"','&quot;',$xmlStr);
$xmlStr=str_replace("'",'&#39;',$xmlStr);
$xmlStr=str_replace("&",'&amp;',$xmlStr);
return $xmlStr;
}

// Opens a connection to a MySQL server
$mysqli = new mysqli("localhost","root","12345678","logix");
if($mysqli->connect_errno){
	echo "Fallo al conectar a MYSQL:". $mysqli->connect_errno .")". $mysqli->connect_error;
}


// Select all the rows in the markers table
$query = "SELECT * FROM cli_clientes WHERE cli_estado=1";
$result = $mysqli->query($query);
if (!$result) {
  die('Invalid query: ' . mysqli_error());
}

header("Content-type: text/xml");

// Start XML file, echo parent node
echo "<?xml version='1.0' ?>";
echo '<markers>';
$ind=0;
// Iterate through the rows, printing XML nodes for each
while ($row = $result->fetch_assoc()){
  // Add to XML document node
  echo '<markerr ';
  echo 'id="' . $row['cli_id'] . '" ';
  echo 'name="' . parseToXML($row['cli_nombre']) . '" ';  
  echo 'lat="' . $row['cli_latitud'] . '" ';
  echo 'lng="' . $row['cli_longitud'] . '" ';  
  echo '/>';
  $ind = $ind + 1;
}

// End XML file
echo '</markers>';

?>